﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simple_calculator.Controls
{
    //синглтон-класс Memory, объект которого можно будет создать только один раз
    sealed class Memory
    {
        //список выполненных операций
        List<Operation> _memory;

        //длина памяти
        int _memLength = 5;

        //конструктор класса, который инициализирует _memory
        Memory()
        {
            _memory = new List<Operation>();
        }

        //специальная функция для создания только одного объекта и обращения только к нему
        internal static Memory Instance { get; } = new Memory();

        //функция для получения списка выполненных операций
        public List<Operation> Get()
        {
            return _memory;
        }

        //функция для сохранения в списке нового результата
        public void Add(Operation op)
        {
            if (_memory.Count < _memLength)
            {
                _memory.Add(op);
            }
            else
            {
                for (int n = 0; n < _memLength-1; n++)
                {
                    _memory[n] = _memory[n + 1];
                }
                _memory[_memLength-1] = op;
            }
        }

        //функция для вывода всех запомненных результатов цельным блоком
        public string Show()
        {
            string memString = "\n";
            foreach (Operation op in _memory)
            {
                memString += op.GetX() + " " + op.GetSymbol() + " " + op.GetY() + " = " + op.GetResult() + "\n";
            }

            return memString;
        }
    }
}
