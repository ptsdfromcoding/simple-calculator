﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simple_calculator.Controls
{
    //служебный статический класс с методами для запроса числа и операции
    //нельзя создать объект класса, потому что статический
    //поэтому нет и конструктора
    static class Getters
    {
        //функция для узнавания числа у пользователя
        public static double GetNumber()
        {
            double number = double.Parse(Console.ReadLine());
            return number;
        }

        //функция для узнавания операции у пользователя
        public static char GetOperation()
        {
            char symbol = char.Parse(Console.ReadLine());
            return symbol;
        }
    }
}
