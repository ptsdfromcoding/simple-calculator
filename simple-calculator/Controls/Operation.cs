﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simple_calculator.Controls
{
    //класс, который может хранить в себе 
    public class Operation
    {
        //внутренние переменные, доступ к которым есть только у этого же класса
        double _x { get; set; }
        double _y { get; set; }
        char _symbol { get; set; }
        double _result { get; set; }

        //конструктор класса
        internal Operation(double n1, double n2, char s)
        {
            _x = n1;
            _y = n2;
            _symbol = s;
        }

        //функции для получения значений внутренних переменных
        public double GetX() { return _x; }
        public double GetY() { return _y; }
        public char GetSymbol() { return _symbol; }
        public double GetResult() { return _result; }

        //функция для подсчета
        public void Calculate()
        {
            switch (_symbol)
            {
                case '+':
                    {
                        _result = _x + _y;
                        break;
                    }

                case '-':
                    {
                        _result = _x - _y;
                        break;
                    }

                case '*':
                    {
                        _result = _x * _y;
                        break;
                    }

                case '/':
                    {
                        _result = _x / _y;
                        break;
                    }

                case '^':
                    {
                        _result = Math.Pow(_x, _y);
                        break;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }
            }
        }
    }
}
