﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace simple_calculator.Controls
{
    //для работы с файлами используются классы File, FileInfo, FileStream, StreamReader, StreamWriter
    //эти классы находятся в библиотеке System.IO
    //File - статический класс, нельзя создать его объект
    //поэтому файл либо хранится в FileStream, либо не сохраняется во внутренней памяти вообще
    //FileInfo - динамический класс, для создания объекта требует файл, информация о котором хранится в объекте

    class Logging
    {
        //путь к файлу с логом
        string _savePath = AppDomain.CurrentDomain.BaseDirectory + "log.txt";
        public string GetPath() { return _savePath; }
        public void SetPath(string sp) { _savePath = sp; }

        FileStream _file;
        StreamWriter _sw;

        public Logging()
        {
            _file = new FileStream(_savePath, FileMode.Append, FileAccess.Write);
        }

        public void Log(string str)
        {
            _sw.WriteLine(str);
        }

        public void Log(Operation op)
        {
            _sw.WriteLine(op.GetX() + " " + op.GetSymbol() + " " + op.GetY() + " = " + op.GetResult());
        }

        public void Start()
        {
            _sw = new StreamWriter(_file, Encoding.GetEncoding(1251));

            _sw.WriteLine("----- {0} -----", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
        }

        public void End()
        {
            _sw.Close();
        }
    }
}
