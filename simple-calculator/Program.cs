﻿using simple_calculator.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class Program
    {
        static void Main()
        {
            //изменяем кодировку в консоли для вывода кириллицы
            Console.OutputEncoding = Encoding.GetEncoding(1251);

            //переменные для хранения двух чисел для операции
            Operation op;

            //переменная с ссылкой на инстанс - единственный объект - синглтон-класса Memory
            Memory memory = Memory.Instance;

            //логгирование
            Logging logHandler = new Logging();
            logHandler.Start();

            //переменная для выхода из цикла
            bool repeatCycle = true;

            //переменная для выхода из приложения
            bool quit;

            //создаем цикл для непрерывной работы с калькулятором
            while (repeatCycle)
            {
                quit = true;
                //пытаемся выполнить подсчеты и ловим ошибки на ходу
                try
                {
                    //спрашиваем первое число
                    Console.WriteLine("Введите первое число, символ операции и второе число через Enter");
                    double x = Getters.GetNumber();
                    char s = Getters.GetOperation();
                    double y = Getters.GetNumber();
                    op = new Operation(x, y, s);

                    //подсчет в зависимости от операции
                    op.Calculate();

                    //запись в список результатов новый результат
                    memory.Add(op);
                    logHandler.Log(op);

                    //вывод текущего ответа и прошлых результатов
                    Console.WriteLine("Ответ: " + op.GetResult());
                    Console.Write("Прошлые результаты:" + memory.Show());
                    Console.WriteLine();
                }
                //если код операции не найден, то поймать ошибку
                catch (ArgumentOutOfRangeException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 001: Операция не обнаружена, введите заново.");
                }
                //если при подсчетах с индексом в массиве ошибка, то поймать
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine("ERROR 002: " + ex.Message);
                }
                catch (InvalidCastException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 003: Не удалось обработать число или операцию, введите заново.");
                }
                catch (FormatException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 004: Некорректный ввод, введите заново.");
                }
                //если какая-то другая ошибка, то поймать
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR CMN: " + ex.Message);
                }
                finally
                {
                    //спрашиваем пользователя, хочет ли он выйти из приложения
                    while (quit)
                    {
                        Console.WriteLine("Продолжить? y/n");
                        try
                        {
                            switch (char.Parse(Console.ReadLine()))
                            {
                                case 'y':
                                    quit = false;
                                    break;
                                case 'n':
                                    quit = false;
                                    repeatCycle = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (InvalidCastException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine("ERR 005: Введено больше одного символа.");
                        }
                    }
                }
            }

            logHandler.End();
        }
    }
}